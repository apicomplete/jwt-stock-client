<?php

namespace App\Http\Controllers;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request){
        $request->validate([
           'email' => 'required|email|max:250',
            'password' => 'required|string|min:8|max:250',
        ]);

        $response = Curl::to('http://192.168.1.12:8000/api/user/login')
            ->withData([
                'email'=> $request->email,
                'password'=> $request->password
            ])
            ->asJson()
            ->post();
        $api_auth = $response->currentUser;
        session()->put(compact('api_auth'));

        //dd(session()->get('api_auth'));
    return redirect('dashboard');

    }
}
