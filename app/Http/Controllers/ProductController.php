<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class ProductController extends Controller
{

//reccuperation des categories et affichage
    public function showCategories(){

        $categories = Curl::to('http://192.168.1.12:8000/api/categories')
            ->withData([
                'token'=> session()->get('api_auth')->token,

            ])
            ->asJson()
            ->get();


        return view('addProd',compact('categories'));

    }

    public function detailProduct ($id)
    {
        
        $data= Curl::to('http://192.168.1.12:8000/api/products')
            ->withData([
                'token'=> session()->get('api_auth')->token,
                'id'=>$id,
            ])
            ->asJson()
            ->get();
            $products = $data[0];
         return view('detail',compact('products'));

    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|string|max:250',
            'price' => 'required|integer|min:10',
            'quantity' => 'required|integer|min:1',
            'categories' => 'required',
            'description' => 'required|string|max:250',
        ]);
        Curl::to('http://192.168.1.12:8000/api/products')
            ->withData([

                'token'=> session()->get('api_auth')->token,
                'name'=> $request->name,
                'price'=> $request->price,
                'category_id'=> $request->categories,
                'quantity'=> $request->quantity,
                'description'=> $request->description,
            ])
            ->asJson()
            ->Post();
        return redirect()->back()->with('success','Produit enregistrer avec succès!');

    }

    public function product_list ()
    {

        $products= Curl::to('http://192.168.1.12:8000/api/products')
            ->withData([
                'token'=> session()->get('api_auth')->token,
            ])
            ->asJson()
            ->get();
        return view('dashboard',compact('products'));

    }

        public function editProduct (){
            Curl::to('http://192.168.1.12:8000/api/products')
            ->withData([

                'token'=> session()->get('api_auth')->token,
                'name'=> $request->name,
                'price'=> $request->price,
                'category_id'=> $request->categories,
                'quantity'=> $request->quantity,
                'description'=> $request->description,
            ])
            ->asJson()
            ->put();
        return redirect()->back()->with('success','Produit modifié avec succès!');
            
        }

       public function deleteProduct($id){

        $response=Curl::to('http://192.168.1.12:8000/api/products')
            ->withData([

                'token'=> session()->get('api_auth')->token,
                'id'=>$id,

            ])
            ->asJson()
            ->delete();
            dd($response);
        return redirect()->back()->with('success','Produit supprimé avec succès!');

       }



}
