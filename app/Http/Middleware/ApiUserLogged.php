<?php

namespace App\Http\Middleware;

use Closure;

class ApiUserLogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if(!session()->has('api_auth')){
           return redirect('login');
       }
        return $next($request);
    }

}
