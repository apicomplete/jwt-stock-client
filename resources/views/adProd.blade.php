
<h1>Liste des etudiants</h1>
<hr>

<div class="row">
    <div class="col-sm-6">

        <form action="{{url('add')}}" method="POST">
            @csrf
            <div class="form-group">
                <label  class="control-label">Catégorie </label>
                <select name="categorie" class="custom-select">
                    @foreach($categories as $category)

                        <option value="{{$category->id}}">{{$category->name}}</option>

                    @endforeach

                </select>

            </div>
            <div class="form-group">
                <label for="name" class="control-label">Name </label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Inserer le nom du produit" value="{{ old('name') }}">
            </div>


            <div class="form-group">
                <label for="quantity" class="control-label">Quantity </label>
                <input type="text" name="quantity" id="quantity" class="form-control" placeholder="Quantité du produit" value="{{ old('quantity') }}">
            </div>
            <div class="form-group">
                <label for="price" class="control-label">Price</label>
                <input type="text" name="price" id="price" class="form-control" placeholder="Prix du produit" value="{{ old('price') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>

        </form>

    </div>
    <div class="col-sm-6">

        @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif


        @if ($errors->any())
            <div class="alert alert-danger">
                <h4>Erreurs</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

</div>