@extends('layouts.app')

@section('content')

<table>
<tr>
<td width="400px">
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Menu</div>

                    <div class="card-body">
                        <ol><a href="">Liste des Produits</a></ol>
                        <ol><a href={{ url('addProd') }}>Ajouter un Produit</a></ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
<td>
<td width="1000px">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Liste des Produits</div>

                    <div class="card-body">
                        
                    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{$product->name}}</td>
            <td>{{$product->price}}</td>
            <td>{{$product->quantity}}</td>
            <td>
                    <a href="{{ url('product-details/'.$product->id) }}" class="btn btn-sm btn-primary">Détails</a>
                    <a href="{{ url('edit-product/'.$product->id) }}" class="btn btn-sm btn-warning">Modifier</a>
                    <button type="button" onclick="event.preventDefault();confirm('Êtes vous sûr de vouloir supprimer ce produit ?') && document.querySelector('#delete-form-{{ $product->id }}').submit()" class="btn btn-sm btn-danger">Supprimer</button>
                    <form action="{{ url('delete-product/'.$product->id) }}" method="POST" id="delete-form-{{ $product->id }}">
                        @method('DELETE')
                        @csrf
                    </form>
                    
                </td>
        </tr>

        @endforeach
        </tbody>
    </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</td>
</table>
@endsection
