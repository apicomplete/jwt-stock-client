@extends('layouts.app')

@section('content')

<h1>Details du produit</h1>
    <hr>

    @if (session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Product's detail</h3>
        </div>
        <div class="card-body">
            <h4><strong>Name : </strong>{{$products->name}}</h4>
            <h4><strong>Price : </strong>{{$products->price}}</h4>
            <h4><strong>Quantity : </strong>{{$products->quantity}}</h4>
            <h4><strong>Description : </strong></h4>
            <hr>
            <p>
                {{$products->description}}
            </p>
        </div>
        

        <div class="card-footer">
            <a href="{{ url('dashboard') }}" class="btn btn-dark">Voir tous les produits</a>
            <a href="{{ url('edit-product/'.$products->id) }}" class="btn btn-warning">Modifier le produit</a>
            <button type="button" onclick="event.preventDefault();confirm('Êtes vous sûr de vouloir supprimer ce produit ?') && document.querySelector('#delete-form-{{ $products->id }}').submit()" class="btn   btn-danger">Supprimer ce produit</button>
            <form action="{{ url('delete-product/'.$products->id) }}" method="POST" id="delete-form-{{ $products->id }}">
                @method('DELETE')
                @csrf
            </form>

        </div>
    </div>
    @endsection