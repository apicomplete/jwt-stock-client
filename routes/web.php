<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Ixudra\Curl\Facades\Curl;

//Auth::routes();
Route::post('login', 'LoginController@login');

Route::get('login', function(){
    return view('auth.login');
});


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'api_logged'], function (){

    Route::get('addProd','ProductController@showCategories');
    Route::get('dashboard','ProductController@product_list');
    Route::get('product-details/{id}','ProductController@detailProduct');
    Route::post('add-product','ProductController@store');
    Route::put('edit-product','ProductController@editProduct');
    Route::delete('delete-product/{id}','ProductController@deleteProduct');

});

